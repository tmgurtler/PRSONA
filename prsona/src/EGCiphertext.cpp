#include "EGCiphertext.hpp"

EGCiphertext::EGCiphertext()
{ /* */ }

EGCiphertext::EGCiphertext(
    const Twistpoint& mask,
    const Twistpoint& encryptedMessage)
: mask(mask), encryptedMessage(encryptedMessage)
{ /* */ }

bool EGCiphertext::operator==(
    const EGCiphertext& other) const
{
    return mask == other.mask && encryptedMessage == other.encryptedMessage;
}

std::ostream& operator<<(
    std::ostream& os,
    const EGCiphertext& output)
{
    os << output.mask << output.encryptedMessage;

    return os;
}

std::istream& operator>>(
    std::istream& is,
    EGCiphertext& input)
{
    is >> input.mask >> input.encryptedMessage;

    return is;
}
