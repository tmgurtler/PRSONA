#ifndef __PROOF_HPP
#define __PROOF_HPP

#include <openssl/evp.h>
#include <openssl/sha.h>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>

#include "Scalar.hpp"
#include "Curvepoint.hpp"
#include "Bipoint.hpp"

class Proof {
    public:
        Proof()
        { /* */ }

        Proof(
            std::string hbc)
        : hbc(hbc)
        { /* */ }

        void clear();
    
        // HBC security
        std::string hbc;

        // Malicious security
        std::vector<Twistpoint> curvepointUniversals;
        std::vector<TwistBipoint> curveBipointUniversals;
        std::vector<Scalar> challengeParts;
        std::vector<Scalar> responseParts;

        bool operator==(
            const Proof& b);

        friend std::ostream& operator<<(
            std::ostream& os,
            const Proof& output);
        friend std::istream& operator>>(
            std::istream& is,
            Proof& input);
};

Scalar oracle(
    const std::string& input,
    size_t lambda_0 = 256);

#endif

