#ifndef __EG_CIPHERTEXT_HPP
#define __EG_CIPHERTEXT_HPP

#include "Curvepoint.hpp"

class EGCiphertext {
    public:
        EGCiphertext();
        EGCiphertext(
            const Twistpoint& mask,
            const Twistpoint& encryptedMessage);
        Twistpoint mask;
        Twistpoint encryptedMessage;

        bool operator==(
            const EGCiphertext& other
        ) const;
        friend std::ostream& operator<<(
            std::ostream& os,
            const EGCiphertext& output);
        friend std::istream& operator>>(
            std::istream& is,
            EGCiphertext& input);
};

#endif