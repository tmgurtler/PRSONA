#!/bin/sh

servers=$(cat cfg/$1/serverIPs.cfg)
clients=$(cat cfg/$1/clientIPs.cfg)

for server in $servers
do
    curl $server/exit
done

for client in $clients
do
    curl $client/exit
done
